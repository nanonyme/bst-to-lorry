# Copyright 2020, 2021 freedesktop-sdk
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# This file is a modified derivative of the Collect Manifest plugin,
# released under the same license
#
# Authors:
#        Valentin David <valentin.david@gmail.com> (Collect Manifest)
#        Adam Jones <adam.jones@codethink.co.uk>   (Collect Manifest)
#        Douglas Winship <douglas.winship@codethink.co.uk> (Url Manifest)
#        Abderrahim Kitouni <akitouni@gnome.org> (Buildstream plugin -> script)

# A script used to produce a manifest file containing a list of source urls for
# a given dependency, and all its subdependencies, down to the bottom of the
# tree.
#
# The manifest contains information such as:
#     - the alias used in the url (null if no alias is used)
#     - the 'main' source url
#
# The manifest file is exported as a json file to the path provided
# under the "path" variable defined in the .bst file.
#
# We'd also like to include the list of mirror urls (which may be empty).

import json
import glob
import os
import re
import sys
import urllib.parse
import argparse
import yaml
import posixpath # useful for url paths
from collections import namedtuple

from buildstream._frontend.app import App
from buildstream.types import _PipelineSelection

def simplify_glob(track):
    "Replace a glob expression with a simpler one that only contains (at most) one asterisk"
    track = track.replace("?", "*")
    track = re.sub(r"\[[^\]]+\]", "*", track)
    if track.count("*") > 1:
        # drop everything between the first and last asterisk
        track = track[:track.index("*")] + track[track.rindex("*"):]
    return track

def extract_alias(source, raw_url):
    translated_url = source.translate_url(raw_url, primary=False)
    if raw_url == translated_url:
        return None, None, translated_url

    alias, rest = raw_url.split(":", 1)
    return alias, translated_url.removesuffix(rest), translated_url

def gen_file_entry(source, raw_url, suffix=''):
    alias, alias_url, url = extract_alias(source, raw_url)
    if suffix:
        url = posixpath.join(url, suffix)

    return dict(type="file", alias=alias, alias_url=alias_url, url=url)

def gen_git_entry(source, raw_url, track=None):
    alias, alias_url, translated_url = extract_alias(source, raw_url)

    refspecs = []
    if track:
        track = simplify_glob(source.tracking)
        if track == "*":
            refspecs = []
        elif track.startswith("refs/") or "*" not in track:
            refspecs = [track]
        else:
            refspecs = [f"refs/heads/{track}", f"refs/tags/{track}"]

    return dict(type="git", alias=alias, alias_url=alias_url, url=translated_url, refspecs=refspecs)

def get_source_locations(sources):
    """
    Returns a list of source URLs and refs, currently for
    git, tar, ostree, remote, zip and tar sources.
    Patch sources are not included in the output, since
    they don't have source URLs

    :sources A list or generator of BuildStream Sources
    """
    source_locations = []
    for source in sources:
        source_kind = source.get_kind()
        if source_kind in ("local", "patch", "patch_queue", "live_bootstrap_prepare"):
            # Path inside the project
            continue
        elif source_kind == 'cargo':
            for crate in source.get_source_fetchers():
                path = "{name}/{name}-{version}.crate".format(name=crate.name, version=crate.version)
                source_locations.append(gen_file_entry(source, source.url, path))
        elif source_kind in ('tar', 'zip', 'remote'):
            source_locations.append(gen_file_entry(source, source.original_url))
        elif source_kind == 'pypi':
            source_locations.append(gen_file_entry(source, source.base_url, source.fetcher.suffix))
        elif source_kind == 'cpan':
            source_locations.append(gen_file_entry(source, source.orig_index, source.fetcher.suffix))
        elif source_kind == 'live_bootstrap_manifest':
            for entry in source.get_source_fetchers():
                source_locations.append(gen_file_entry(source, entry.url))
        elif source_kind == 'git_repo':
            source_locations.append(gen_git_entry(source, source.url, source.tracking))
        elif source_kind in ('git', 'git_tag', 'git_module'):
            for mirror in source.get_source_fetchers():
                source_locations.append(gen_git_entry(source, mirror.url))
        elif source_kind == "docker":
            # Ignore for now
            pass
        else:
            sys.exit(f"ERROR: unhandled source plugin {source_kind}")

    return source_locations

GitLorry = namedtuple("GitLorry", ("name", "url", "refspecs"))
RawFileEntry = namedtuple("RawFileEntry", ("destination", "url"))

# Write git lorry files
def process_git(source_url, alias_url):
    path = source_url.removeprefix(alias_url)
    return path.removesuffix(".git")

# Write raw-file lorry files
def process_raw_file(source_url, alias_url):
    path = source_url.removeprefix(alias_url)
    return os.path.dirname(path)

# Write lorry file
def write_file(alias, entries, kind, file_path):
    with open(file_path, 'w') as file:
        if kind == "git":
            data = {}
            for lorry in sorted(entries):
                data[f"{alias}/{lorry.name}"] = {"type": "git", "url": lorry.url}
                if lorry.refspecs:
                    data[f"{alias}/{lorry.name}"]["refspecs"] = list(lorry.refspecs)
            yaml.dump(data, file, sort_keys=False)
        elif kind == "raw-file":
            urls = []
            for entry in sorted(entries):
                if entry.destination:
                    urls.append({"destination": entry.destination, "url": entry.url})
                else:
                    urls.append({"url": entry.url})
            data = {f"{alias}/raw-files": {"type": "raw-file", "urls": urls}}
            yaml.dump(data, file)

def extract_manifest(data, git_dir, raw_files_dir):
    aliases = set()

    # Dicts to hold results
    git_result = {}
    raw_file_result = {}
    lorry_files = set(glob.glob(os.path.join(git_dir, '*.lorry')))
    lorry_files.update(glob.glob(os.path.join(raw_files_dir, '*.lorry')))

    # Load lorry files
    for filepath in lorry_files:
        with open(filepath, 'r') as file:
            content = yaml.safe_load(file)

        alias = os.path.basename(filepath).removesuffix('.lorry')
        aliases.add(alias)

        for k, v in content.items():
            if v["type"] == "git":
                git_result.setdefault(alias, set())
                name = k.removeprefix(alias + "/")
                url = v['url']
                refspecs = tuple(v.get('refspecs', []))
                git_result[alias].add(GitLorry(name, url, refspecs))

            elif v["type"] == "raw-file":
                raw_file_result.setdefault(alias, set())
                for entry in v["urls"]:
                    destination = entry.get('destination', '')
                    url = entry['url']
                    raw_file_result[alias].add(RawFileEntry(destination, url))

    # Loop through each entry within manifest.json
    for entry in data:
        for source in entry['sources']:
            typ = source['type']
            url = source['url']
            alias = source['alias']
            alias_url = source['alias_url']

            if alias_url is None:
                continue

            aliases.add(alias)

            if typ == "git":
                alias_result = git_result.setdefault(alias, set())

                name = process_git(url, alias_url)
                alias_result.add(GitLorry(name, url, tuple(source["refspecs"])))

            elif typ in "file":
                alias_result = raw_file_result.setdefault(alias, set())

                destination = process_raw_file(url, alias_url)
                alias_result.add(RawFileEntry(destination, url))

            else:
                print(f"WARNING: unhandled mirror type {typ}")

    # Write lorry file
    for alias in aliases:
        if alias in git_result and alias in raw_file_result:
            sys.exit(f"ERROR: Alias {alias} is used for both git repositories and raw files")

        if alias in git_result:
            filepath = os.path.join(git_dir, f"{alias}.lorry")
            write_file(alias, git_result[alias], "git", filepath)

        if alias in raw_file_result:
            filepath = os.path.join(raw_files_dir, f"{alias}.lorry")
            write_file(alias, raw_file_result[alias], "raw-file", filepath)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Write Lorry files based on bst requirements")
    parser.add_argument("elements", metavar="element", nargs="+", help="Element files to process")
    parser.add_argument("--git-directory", dest="git_dir", default=".", help="Directory to write git lorry files to")
    parser.add_argument("--raw-files-directory", dest="raw_files_dir", default=".", help="Directory to write raw-files lorry files to")
    args = parser.parse_args()

    # Ensure output directory exists
    for directory in [args.git_dir, args.raw_files_dir]:
        os.makedirs(directory, exist_ok=True)


    manifest = []
    visited_names_list = set()

    app = App.create({'no_interactive': True, 'colors': True, 'directory': '', 'config': '', 'log_file': '', 'option':''})

    with app.initialized():
        for dep in app.stream.load_selection(args.elements, selection=_PipelineSelection.ALL):
            if dep.name in visited_names_list:
                continue
            visited_names_list.add(dep.name)

            sources = get_source_locations(dep.sources())
            if sources:
                manifest.append({'element': dep.name, 'sources': sources})

    extract_manifest(manifest, args.git_dir, args.raw_files_dir)
